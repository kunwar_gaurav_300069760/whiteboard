export const menus = [
    {
        name: 'pencil',
        type: 'source-over',
        value: '#46545f',
        lineWidth: 1,
        subMenus: [{
            key: 'submenu-option',
            name: 'stroke1',
            lineWidth: 1,
        }, {
            key: 'submenu-option',
            name: 'stroke3',
            lineWidth: 3,
        }, {
            key: 'submenu-option',
            name: 'stroke5',
            lineWidth: 5,
        }, {
            key: 'submenu',
            name: 'stroke-primary',
            color: 'primary',
            value: '#46545f'
        }, {
            key: 'submenu',
            name: 'stroke-red',
            color: 'red',
            value: 'red'
        }, {
            key: 'submenu',
            name: 'stroke-green',
            color: 'green',
            value: 'green'
        }, {
            key: 'submenu',
            name: 'stroke-yellow',
            color: 'yellow',
            value: 'yellow'
        }]
    },
    {
        name: 'highlighter',
        value: 'rgb(70, 84, 95, 0.3)',
        type: 'source-over',
        lineWidth: 5,
        subMenus: [{
            key: 'submenu',
            name: 'stroke-primary',
            color: 'primary',
            value: '#46545f'
        }, {
            key: 'submenu',
            name: 'stroke-red',
            color: 'rgb(255, 0, 0, 0.5)',
            value: 'rgb(255, 0, 0, 0.5)'
        }, {
            key: 'submenu',
            name: 'stroke-green',
            color: 'rgb(0, 255, 0, 0.5)',
            value: 'rgb(0, 255, 0, 0.5)'
        }, {
            key: 'submenu',
            name: 'stroke-yellow',
            color: 'rgb(255, 255, 0, 0.5)',
            value: 'rgb(255, 255, 0, 0.5)'
        }]
    },
    {
        name: 'eraser',
        type: 'destination-out',
        lineWidth: 10
    }
]