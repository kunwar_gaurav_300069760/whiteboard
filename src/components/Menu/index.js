import React from 'react';
import './menu.css';

function Menu(props) {
    let subMenu = null, subMenuOptions = null;
    if (props.tool.subMenus) {
        subMenuOptions = props.tool.subMenus.filter(menu => menu.key === 'submenu-option');
        subMenu = props.tool.subMenus.filter(menu => menu.key === 'submenu');
    }

    return (
        <section className="menu">
            <div className="menu-container">
                {props.menus.map(menu => {
                    return (<div key={menu.name}>
                        <button
                            onClick={() => props.selectTool(menu)}
                            className={props.tool.name === menu.name ? 'active' : ''}>
                            <img
                                alt="Draw Tool"
                                src={`./public/${menu.name}.svg`}
                                draggable="false" />
                        </button>
                    </div>);
                })}
            </div>
            {subMenu && <div className="submenu-container">
                <div>
                    {subMenu.map(subMenu => {
                        const isActive = props.tool.value === subMenu.value;
                        const style = isActive && subMenu.color ? { filter: 'none' } : {};
                        return (<div key={subMenu.name}>
                            <button
                                onClick={() => {
                                    const options = { ...subMenu };
                                    props.modifyTool({ ...options })
                                }}
                                className={isActive ? 'active' : ''}>
                                <img
                                    style={style}
                                    alt="Draw Tool"
                                    src={`./public/${subMenu.name}.svg`}
                                    draggable="false" />
                            </button>
                        </div>);
                    })}
                </div>
                <div>
                    {subMenuOptions && subMenuOptions.map(subMenu => {
                        const isActive = props.tool.lineWidth === subMenu.lineWidth;
                        const style = isActive && subMenu.color ? { filter: 'none' } : {};
                        return (<div className="submenu-options" key={subMenu.name}>
                            <button
                                onClick={() => {
                                    const options = { ...subMenu };
                                    props.modifyTool({ ...options })
                                }}
                                className={isActive ? 'active' : ''}>
                                <img
                                    style={style}
                                    alt="Draw Tool"
                                    src={`./public/${subMenu.name}.svg`}
                                    draggable="false" />
                            </button>
                        </div>);
                    })}
                </div>
            </div>}
        </section>
    )
}

export default Menu;